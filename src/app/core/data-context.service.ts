import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpRequest, HttpEvent, HttpEventType, HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataContextService {
   constructor(private httpClient: HttpClient) { }
   
   public putWithProgress<T>(url: string, data:any): Promise<any>{
       url = this.processUrl(url);
       console.log('sent', data);
       const req  = new HttpRequest('POST', url, data, {
           reportProgress: true,
        //    headers: this.getHeaders()
       });
       console.log(req);

       return new Promise<any>((resolve, reject)=>{
        this.httpClient.request(req).subscribe(
            (response: HttpEvent<Object>) => {
                    if(response.type === HttpEventType.DownloadProgress){
                   //    let total = response.total;
                   //    let toLoad = response.loaded;
                   //    this.showProgress.next(Math.round(100 * toLoad / total));
                    }
                    if(response.type === HttpEventType.Response){
                        resolve(response.body);
                    }
            }, err =>{
                reject(err);
            });
       });
   }
   public getWithProgress<T>(url:string): Promise<any>{
     url = this.processUrl(url);
         const req  = new HttpRequest('GET', url, {
             reportProgress:true,
            //  headers: this.getHeaders()
         });
        return new Promise<any>((resolve, reject)=>{
         this.httpClient.request(req).subscribe(
             (response: HttpEvent<Object>) => {
                     if(response.type === HttpEventType.DownloadProgress){
                    //    let total = response.total;
                    //    let toLoad = response.loaded;
                    //    this.showProgress.next(Math.round(100 * toLoad / total));
                     }
                     if(response.type === HttpEventType.Response){
                         resolve(response.body);
                     }
             }, err =>{
                 reject(err);
             });
        });
  }
    private getHeaders() {
        let headers = new HttpHeaders().set("Accept", "application/json");
            // headers = new HttpHeaders().set('Authorization', 'Bearer ' + this.identity.Token);
        return headers;
    }
     private processUrl(url: string) {
      if (!url || url === '')
          return url;
      if (url.toLowerCase().indexOf('http://') !== 0 && url.toLowerCase().indexOf('https://') !== 0)
          url = environment.apiHost + url;

      console.log(url);
      return url;
    }
}