import { Component, OnInit } from '@angular/core';
import { DataContextService } from '../core/data-context.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private data: DataContextService){}
  title = 'morsella';
  roles: [];
  ngOnInit(){
    this.data.getWithProgress('/user/roles').then(res =>{
      console.log(res);
      this.roles = res.roles;
    });
  }
}
