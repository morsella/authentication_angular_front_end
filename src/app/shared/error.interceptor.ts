import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpErrorResponse,
    HttpEvent
  } from "@angular/common/http";
  import { catchError } from "rxjs/operators";
  import { throwError } from "rxjs";
  import { Injectable, Injector } from "@angular/core";
import { Router } from '@angular/router';
  
//   import { ErrorComponent } from "./error/error.component";
//   import { ErrorService } from "./error/error.service";
  
  @Injectable()
  export class ErrorInterceptor implements HttpInterceptor {
  
    constructor(private _injector: Injector) {}
  
    intercept(req: HttpRequest<any>, next: HttpHandler){
      return next.handle(req).pipe(
        catchError((error: HttpErrorResponse) => {
          let errorMessage = "An unknown error occurred!";
          if (error.error.message) {
            errorMessage = error.error.message;
          }
          if (error.status === 404) {
            const router = this._injector.get(Router);
            router.navigate(['/404']);
            localStorage.removeItem("token");
            localStorage.removeItem("expiration");            
          }
            console.log(error.error);
          // this.errorService.throwError(errorMessage);
          return throwError(error);
        })
      );
    }
  }