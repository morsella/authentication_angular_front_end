import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OpacityMouseoverDirective} from './opacity-mouseover.directive';
import { WindowViewWidthDirective } from './window-view-width.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    OpacityMouseoverDirective,
    WindowViewWidthDirective
  ],
  exports:[
    OpacityMouseoverDirective
  ]
})

export class SharedModule { }