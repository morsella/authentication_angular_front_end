import { Directive, HostBinding, HostListener, Input, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appOpacityMouseover]'
})
export class OpacityMouseoverDirective {
  constructor(private renderer: Renderer2) { }
  @Input() showItem : ElementRef;
  // @HostBinding('class.opacity') showOpacity = false;
  @HostListener('mouseover',['$event']) toggleOpacity(event: any){

    // this.displayNone(event.target.innerWidth, this.maxWidth, this.hasId);
  }
  // @HostListener('click') toggleDisplay(){
  //   this.displayNone(window.innerWidth, this.maxWidth, this.hasId);
  // }
  ngOnInit(){
    // this.displayNone(window.innerWidth, this.maxWidth, this.hasId);
  }
}
