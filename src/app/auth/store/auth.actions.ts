import { Action } from '@ngrx/store';

export const TRY_SIGNUP = 'TRY_SINGUP';
export const SET_TOKEN = 'SET_TOKEN';
export const SIGN_UP = 'SIGN_UP';
export const LOG_IN = 'LOG_IN';
export const TRY_UPLOAD = 'TRY_UPLOAD';
export const UPLOAD_IMG = 'UPLOAD_IMG';
export const LOG_OUT = 'LOG_OUT';
export const AUTO_AUTH_USER = 'AUTO_AUTH_USER';
export const AUTO_AUTHORIZED = 'AUTO_AUTHORIZED';
export const GET_USER_DATA = 'GET_USER_DATA';
// export const CONFIRM_EMAIL = 'CONFIRM_EMAIL';
export const TRY_CONFIRM_EMAIL = 'TRY_CONFIRM_EMAIL';

export class TryConfirmEmail implements Action{
    readonly type = TRY_CONFIRM_EMAIL;
}

// export class ConfirmEmail implements Action{
//     readonly type = CONFIRM_EMAIL;
//     constructor(public payload: {
//         emailToken: any
//     }) {}
// }

export class GetUserData implements Action{
    readonly type = GET_USER_DATA;
    constructor(public payload:{
        confirmed: boolean
    }){}
}

export class Authorized implements Action{
    readonly type = AUTO_AUTHORIZED;
    constructor(public payload: {
        token: string,
        expiresIn: any
    }){}
}

export class AutoAuthUser implements Action{
    readonly type = AUTO_AUTH_USER;
}

export class TrySignup implements Action {
    readonly type= TRY_SIGNUP;
    constructor(public payload: {
        username: string, 
        firstName: string,
        lastName: string,
        password: string, 
        confirmPassword: string,
        email: string
        // image: any,
    }){}
}
export class TryUpload implements Action{
    readonly type = TRY_UPLOAD;
    constructor(public payload:{
        image: any
    }){}
}
export class UploadImg implements Action{
    readonly type = UPLOAD_IMG;
}
export class SetToken implements Action {
    readonly type = SET_TOKEN;
    constructor(public payload: {
        token: string
    }){}
}
export class Signup implements Action {
    readonly type = SIGN_UP;
}
export class Login implements Action {
    readonly type = LOG_IN;
}
export class Logout implements Action {
    readonly type = LOG_OUT;
}
export type AuthActions  = 
  UploadImg 
| TryUpload 
| TrySignup 
| SetToken 
| Signup 
| Login 
| Logout 
| AutoAuthUser 
| Authorized
| GetUserData
// | ConfirmEmail
| TryConfirmEmail;