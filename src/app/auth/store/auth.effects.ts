import { Actions, Effect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { map, tap, switchMap, mergeMap} from 'rxjs/operators';
import * as AuthActions from './auth.actions';
import { DataContextService } from 'src/app/core/data-context.service';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducers';




@Injectable()
export class AuthEffects {
    constructor(
        private actions$: Actions, 
        private router: Router,
        private dataContext: DataContextService,
        private store: Store<fromApp.AppState>
    ){}
    private tokenTimer: any;
    private saveAuthData(token: string, expirationDate: Date) {
      localStorage.setItem("token", token);
      localStorage.setItem("expiration", expirationDate.toISOString());
      console.log('set', localStorage);
    }
  
    private clearAuthData() {
      localStorage.removeItem("token");
      localStorage.removeItem("expiration");
      console.log('clear',localStorage);
    }
    private setAuthTimer(duration: number) {
      console.log("Setting timer: " + duration);
      this.tokenTimer = setTimeout(() => {
        console.log('time left',duration);
        this.store.dispatch(new AuthActions.Logout());
      }, duration * 1000);
    }
    private getAuthData() {
      console.log('get auth');
      const token = localStorage.getItem("token");
      const expirationDate = localStorage.getItem("expiration");
      if (!token || !expirationDate) {
        return;
      }
      return {
        token: token,
        expirationDate: new Date(expirationDate)
      }
    }

    // @Effect()
    // confirmEmail = this.actions$.pipe(ofType(AuthActions.CONFIRM_EMAIL))
    // .pipe(map((action: AuthActions.ConfirmEmail)=>{
    //   console.log('confirmed email 1', action.payload);
    //   return action.payload;
    // }),switchMap((payload)=>{
    //   console.log('confirm payload 2', payload);
    //   return this.dataContext.getWithProgress('/user/confirm-email/'+payload);
    // }),mergeMap((res)=>{
    //   console.log(res);
    //   return [
    //     {
    //       type: AuthActions.LOG_IN
    //     }
    //   ]
    // }));

    @Effect()
    getUserData = this.actions$.pipe(ofType(AuthActions.GET_USER_DATA))
    .pipe(map((action: AuthActions.GetUserData) => {
      console.log('action user data', action.payload);
      return action.payload;
    })
    , switchMap(() => {
      return this.dataContext.getWithProgress('/user/user-data');
    }),mergeMap((res)=>{
      console.log('user data', res);
      if(res.confirmed){
        return [
          {
            type: AuthActions.LOG_IN
          }
        ]
      }else{
        return [
          {
            type: AuthActions.TRY_CONFIRM_EMAIL
          }
        ]
      }
    }));

    @Effect()
    autoAuth = this.actions$.pipe(ofType(AuthActions.AUTO_AUTH_USER),
      map( () => {
        console.log('start', localStorage);
        const authInformation = this.getAuthData();
        if (!authInformation) {
          console.log('no auth', authInformation);
          // this.router.navigate(['/login']);
          let payload = {token: null, expiresIn: null};
          return payload;
        }else{
          const now = new Date();
          const expiresIn = authInformation.expirationDate.getTime() - now.getTime();
          if (expiresIn > 0) {
            let token = authInformation.token;
            this.setAuthTimer(expiresIn / 1000);
            this.router.navigate(['/posts']);
            let payload = {token: token, expiresIn: expiresIn};
            console.log('auth', payload);
            return payload;
          }
        }
    })
    ,mergeMap((response: {token : string, expiresIn: any})=>{
      console.log('1', response);
      if(response.token && response.token.length >0){
        return [
          {
            type: AuthActions.AUTO_AUTHORIZED,
            payload:{
              token: response.token
            }
          },
          {
            type: AuthActions.GET_USER_DATA
          }
        ];
      }else{
        return [
          {
            // type: AuthActions.TRY_CONFIRM_EMAIL
            type: AuthActions.AUTO_AUTHORIZED,
            payload:{
              token: response.token
            }
          }
        ];
      }
    }));

    @Effect({dispatch: false})
    logOut = this.actions$.pipe(ofType(AuthActions.LOG_OUT),
    tap(()=>{
      clearTimeout(this.tokenTimer);
      this.clearAuthData();
      this.router.navigate(["/login"]);
    }));
    @Effect({dispatch: false})
    tryConfirmEmail = this.actions$.pipe(ofType(AuthActions.TRY_CONFIRM_EMAIL),
    tap(()=>{
      this.router.navigate(['/confirm-email']);
    }));

    @Effect()
    authSignup = this.actions$.pipe(ofType(AuthActions.TRY_SIGNUP))
        .pipe(map((action: AuthActions.TrySignup) => {
          console.log(action.payload);
          return action.payload;
        })
        , switchMap((authData: { 
            username: string, 
            firstName: string,
            lastName: string,
            password: string, 
            confirmPassword: string,
            email: string
            // image: any,
           }) => {
          return this.dataContext.putWithProgress('/user/signup',authData);
        })
        , mergeMap((response: {token: string, message: string, expiresIn: number}) => {
            console.log('response effects', response);
            const expiresInDuration = response.expiresIn;
            this.setAuthTimer(expiresInDuration);
            const token = response.token;
            const now = new Date();
            const expirationDate = new Date(now.getTime() + expiresInDuration * 1000);
            console.log('expiration date', expirationDate);
            this.saveAuthData(token, expirationDate);
          this.router.navigate(['/confirm-email']);
          return [
            {
              type: AuthActions.SET_TOKEN,
                payload: {
                  token: response.token
                }
              },
            {
              type: AuthActions.GET_USER_DATA
            }
          ];
      }));

      @Effect({dispatch: false})
      uploadImg = this.actions$.pipe(ofType(AuthActions.TRY_UPLOAD))
        .pipe(map((action: AuthActions.TryUpload)=>{
          console.log(action.payload);
          return action.payload;
        })
        , switchMap((img)=>{
            console.log(img);
            return this.dataContext.putWithProgress('/upload/image', img);
      }));
}
