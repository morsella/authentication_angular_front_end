import * as AuthActions from './auth.actions';

export interface State {
    token: string, 
    authenticated: boolean
}
const initialState: State = {
    token: null,
    authenticated: false
}

export function authReducer( state = initialState, action: AuthActions.AuthActions){
    switch(action.type){
      case(AuthActions.SIGN_UP):
      case(AuthActions.LOG_IN):
            console.log('state signup, login', state);
            return {
                ...state, 
                authenticated: true
            };
      case (AuthActions.LOG_OUT):
        return {
          ...state,
          token: null,
          authenticated: false
        };
      case(AuthActions.TRY_CONFIRM_EMAIL):
        return {
          ...state,
          authenticated: false
        }
      case (AuthActions.SET_TOKEN):
        return {
          ...state,
          token: action.payload.token
        };
      case (AuthActions.AUTO_AUTHORIZED):
      return {
        ...state,
        token: action.payload.token
      };
      default:
        return state;
    }

    
}