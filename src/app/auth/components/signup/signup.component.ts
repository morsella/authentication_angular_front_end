import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { mimeType } from '../../mime_type.validator';
import { Store } from '@ngrx/store';

import * as fromApp from '../../../store/app.reducers';
import * as AuthActions from '../../store/auth.actions';
import { DataContextService } from 'src/app/core/data-context.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  constructor(private store: Store<fromApp.AppState>, private dataContext: DataContextService) { }
  form: FormGroup;
  imagePreview: string;
  uploadData: any;
  newURL:string;
  noMatch: boolean = false;
  emailValid: boolean = true;

  ngOnInit() {
    this.form = new FormGroup({
      firstName: new FormControl(null, [Validators.required]),
      lastName: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(8), 
        Validators.pattern('(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=[^0-9]*[0-9]).{8,}')]),
      confirmPassword: new FormControl(null, [Validators.required, Validators.minLength(8), 
        Validators.pattern('(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=[^0-9]*[0-9]).{8,}')])
    });
    this.checkPasswords();
    this.checkEmail();
  }
  checkPasswords(): void{
    if(this.form){
      this.form.valueChanges.subscribe(val => {
          if(val.password !== val.confirmPassword){
            this.form.controls['confirmPassword'].setErrors({'incorrect': true});
            this.noMatch = true;
          }else{
            this.form.controls['confirmPassword'].setErrors(null);
            this.noMatch = false;
          }
      });
    }
  }
  checkEmail(): void{
    if(this.form){
      this.form.valueChanges.subscribe(val=>{
        if(val.email){
          let re = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
          this.emailValid= re.test(val.email);
          console.log(this.emailValid);
          if(!this.emailValid){
            this.form.controls['email'].setErrors({'incorrect': true});
          }else{
            this.form.controls['email'].setErrors(null);
          }
        }
      });
    }
  }
  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];    
    this.form.patchValue({ image: file });
    this.form.get("image").updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file);
    this.uploadData = new FormData();
    this.uploadData.append('image', file, file.name);
  }
  onSignup(){
    if (this.form.invalid) {
      return;
    }
    const email = this.form.value.email;
    const firstName = this.form.value.firstName;
    const lastName = this.form.value.lastName;
    const password = this.form.value.password;
    const username = this.form.value.firstName+'_'+this.form.value.lastName;
    const confirmPassword = this.form.value.confirmPassword;
    // const image = this.form.value.image;
    // this.store.dispatch(new AuthActions.TryUpload(this.uploadData));
    // this.dataContext.putWithProgress('/upload/image', this.uploadData).then(res=> {
    //   this.newURL = res.imagUrl;
    // }).then(res=>{
      this.store.dispatch(new AuthActions.TrySignup({
        username: username, 
        firstName: firstName,
        lastName: lastName,
        password: password, 
        confirmPassword: confirmPassword,
        email: email
        // image: this.newURL, 
        }));
    // });
    }
}
