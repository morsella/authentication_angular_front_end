import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducers';
import * as AuthActions from '../auth/store/auth.actions';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss']
})
export class ConfirmEmailComponent implements OnInit {
  constructor(private store: Store<fromApp.AppState>, private route: ActivatedRoute){}
  emailToken: any;
  ngOnInit(){
    // this.route.params
    // .subscribe(
    //   (params: Params) => {
    //     this.emailToken = params['emailToken'];
    //     console.log(this.emailToken);
    //   }
    // );
    // if(this.emailToken){
    //   this.store.dispatch(new AuthActions.ConfirmEmail(this.emailToken));
    // }
  }
}
