import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducers';
import * as AuthActions from '../auth/store/auth.actions';
import { ActivatedRoute, Params } from '@angular/router';
@Component({
  selector: 'app-confirmed',
  templateUrl: './confirmed.component.html',
  styleUrls: ['./confirmed.component.scss']
})
export class ConfirmedComponent implements OnInit {
  constructor(private store: Store<fromApp.AppState>, private route: ActivatedRoute){}
  emailToken: any;
  ngOnInit(){

  }
}
